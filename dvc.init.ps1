$ENV = Get-Content '.env' | ConvertFrom-StringData

poetry run dvc remote add --local -d -f dvc-minio s3://$($ENV.DVC_S3_BUCKET)
poetry run dvc remote modify --local dvc-minio endpointurl http://localhost:9000
poetry run dvc remote modify --local dvc-minio access_key_id $ENV.AWS_ACCESS_KEY_ID
poetry run dvc remote modify --local dvc-minio secret_access_key $ENV.AWS_SECRET_ACCESS_KEY