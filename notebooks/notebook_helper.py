import os
from pathlib import Path


def set_working_folder():
    """Sets notebook working folder at root of the project (pyproject.toml location)
    """
    notebook_path = Path().resolve()
    while not notebook_path.joinpath("pyproject.toml").exists():
        notebook_path = notebook_path.parent
        os.chdir(notebook_path)
    print(f"Current working folder: {Path().resolve()}")
