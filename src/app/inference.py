from typing import List

import mlflow
import pandas as pd
import yaml
from cachetools import TTLCache, cached
from fastapi import FastAPI, HTTPException
from fastapi.responses import FileResponse
from mlflow.entities.model_registry.model_version import ModelVersion
from sklearn.pipeline import Pipeline
from yaml.loader import SafeLoader

from src.app.prediction_response import PredictionResponse
from src.app.session_info_request import SessionInfoRequest
from src.configuration.config import get_config
from src.models import mlflow_helper

app = FastAPI()


# mlflow path to transformer
_transformer_version: ModelVersion = None
# mlflow path to prediction model
_model_version: ModelVersion = None


@app.get("/modelversion")
async def get_model_version() -> FileResponse:
    """Provides information about currently chosen transformer and prediction models.
    Contains etag in response header and can be used as external dependency in DVC
    Returns:
        FileResponse: mlflow paths to file with models info
    """
    # invalidate model
    get_model()
    model_info_path = "modelversion.txt"
    model_info_tag = (
        f"Transformer: {_transformer_version._source}, Predictor: {_model_version._source}"
    )
    # dumping file with information about models
    with open(model_info_path, "w", encoding="utf-8") as f:
        f.write(model_info_tag)
    response = FileResponse(model_info_path)
    response.headers["etag"] = model_info_tag
    return response


@app.post("/predictproba")
async def predict_proba(session_info: List[SessionInfoRequest]) -> List[PredictionResponse]:
    """The method prodcuces prediction for user session using current model from mlflow server

    Args:
        session_info (List[SessionInfoRequest]): list of user sessions

    Returns:
        List[PredictionResponse]: list of predictions
    """
    cfg = get_config()
    if len(session_info) > cfg.model_service.request_max_items:
        raise HTTPException(400, "Too many items for prediction")

    df = pd.DataFrame(list(dict(session) for session in session_info)).set_index(cfg.data.index_col)
    model = get_model()
    prediction = model.predict_proba(df)
    result = [
        PredictionResponse.parse_obj(
            {
                "session_id": df.index.values[i],
                "class_0_proba": prediction[i][0],
                "class_1_proba": prediction[i][1],
            }
        )
        for i in range(len(prediction))
    ]
    return result


@cached(cache=TTLCache(ttl=get_config().model_service.cache_timeout, maxsize=10))
def get_model() -> Pipeline:
    """Get complete pipeline to process raw data into predictions

    Returns:
        Pipeline: sklearn Pipeline instance
    """
    global _transformer_version
    global _model_version
    cfg = get_config()
    mlflow.set_tracking_uri(cfg.mlflow.tracking_uri)
    mlf_client = mlflow.tracking.MlflowClient()
    _model_version = mlflow_helper.get_sklearn_model_version(cfg.mlflow.lr_model_name)
    transformer_tag = yaml.load(_model_version.tags[cfg.mlflow.transformer_tag], SafeLoader)
    _transformer_version = mlf_client.get_model_version(**transformer_tag)
    transformer = mlflow.sklearn.load_model(_transformer_version.source)
    predictor = mlflow.sklearn.load_model(_model_version._source)
    pipe = Pipeline((("transformer", transformer), ("predictor", predictor)))
    return pipe
