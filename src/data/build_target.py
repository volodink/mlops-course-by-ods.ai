import pandas as pd
import click

from src.configuration import get_config


@click.command()
@click.argument("in_train", type=click.Path(exists=True))
@click.argument("out_target", type=click.Path())
def build_target(in_train: str, out_target: str):
    """The method extracts target column from original dataset

    Args:
        in_train (str): path to original dataset (csv)
        out_target (str): path to file with extracted target values
    """
    cfg = get_config()
    df_train = pd.read_csv(in_train)
    y = df_train[cfg.data.target_col]
    y.to_csv(out_target)


if __name__ == "__main__":
    build_target()  # pylint: disable=no-value-for-parameter
