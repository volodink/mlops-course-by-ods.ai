import pandas as pd
import click

from src.configuration import get_config


@click.command()
@click.argument("in_train_raw", type=click.Path(exists=True))
@click.argument("out_train_cleaned", type=click.Path())
def clean_data(in_train_raw: str, out_train_cleaned: str) -> None:
    """Drops rows for the date 2013-04-12.

    EDA shows abnormal target user activity on this date

    Args:
        in_train_raw (str): original input file (csv)
        out_train_cleaned (str): file with rows of 2013-04-12 removed (csv)

    Returns:
        None
    """
    cfg = get_config()

    df_train: pd.DataFrame = pd.read_csv(
        in_train_raw,
        index_col=cfg.data.index_col,
        parse_dates=cfg.data.date_cols,
    )
    df_train = df_train[
        ~(
            (df_train.target == 1)
            & (df_train[cfg.data.date_cols[0]].dt.date == cfg.eda.abnormal_day)
        )
    ]
    df_train.to_csv(out_train_cleaned)


if __name__ == "__main__":
    clean_data()  # pylint: disable=no-value-for-parameter
