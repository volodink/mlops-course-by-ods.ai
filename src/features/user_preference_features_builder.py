from typing import Dict, List, Set

import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin

from src.configuration.config import EDAConfig


class UserPreferenceFeaturesBuilder(BaseEstimator, TransformerMixin):
    """Extracts interest of Alice and the rest of the people
    -------------------
    alice_interested - binary feature, if there is an interesting
        for Alice site in the session
    rest_interested - binary feature, if there is an interesting
        for the rest of the peiople site in the session
    """

    def __init__(
        self, site_columns: List[str], site_dict: Dict[str, str], eda_cfg: EDAConfig
    ):
        self.site_columns = site_columns
        self.site_dict = site_dict
        self.alice_sites: Set[str] = set()
        self.rest_sites: Set[str] = set()
        self.eda_cfg = eda_cfg
        super().__init__()

    def fit(self, X: pd.DataFrame, y=None) -> "UserPreferenceFeaturesBuilder":
        #  Alice themes were observed during EDA
        alice_themes = {
            "ytimg",
            "youtube",
            "video",
            "movie",
            "stream",
            "cinema",
            "film",
            "tv",
            "yt3",
            "youwatch",
            "media",
            "mtv",
            "music",
            "radio",
        }
        alice_themes = set(self.eda_cfg.alice_themes)
        X = X[self.site_columns].applymap(self.site_dict.get, na_action="ignore")
        # get counts of Alice's and rest of the people sites
        alice_site_count = X[y == 1].melt().value.value_counts()
        rest_site_count = X[y == 0].melt().value.value_counts()
        # get top different sites for Alice and rest
        self.rest_sites = set(rest_site_count.head(50).index).difference(
            alice_site_count.index
        )
        alice_site_set = set(alice_site_count.head(40).index).difference(
            rest_site_count.head(250).index
        )
        self.alice_sites = alice_themes.union(
            [
                site
                for site in alice_site_set
                if all(at not in site for at in alice_themes)
            ]
        )
        return self

    def transform(self, X: pd.DataFrame) -> np.ndarray:
        X = X[self.site_columns].applymap(self.site_dict.get, na_action="ignore")
        alice_interested = X.apply(
            self.check_interests, axis=1, args=(self.alice_sites,)
        )
        rest_interested = X.apply(self.check_interests, axis=1, args=(self.rest_sites,))
        return np.c_[alice_interested, rest_interested]

    def check_interests(self, row: pd.Series, interests_list: Set[str]) -> int:
        """The method checks user session to contain interesting sites

        Args:
            row (pd.Series): pandas Series of visited sites in the session
            interests_list (Set[str]): interesting sites

        Returns:
            int: Returns 1 if intereesting site is found, otherwise - 0
        """
        for site in row:
            if isinstance(site, str):
                for interest in interests_list:
                    if interest in site:
                        return 1
        return 0
