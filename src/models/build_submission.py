from typing import List

import click
import pandas as pd
import requests
from pydantic import parse_obj_as

from src.app.prediction_response import PredictionResponse
from src.configuration import get_config


def write_to_submission_file(
    predicted_labels: List[PredictionResponse],
    out_file: str,
    target: str = "target",
    index_label: str = "session_id",
):
    """Turn predictions into data frame and save as csv file (format for Kaggle)

    Args:
        predicted_labels (_type_): array of predicted probabilities for the target
        out_file (_type_): output file path
        target (str, optional): column name with predicted probabilities for the target
        index_label (str, optional): column name with session id
    """
    with open(out_file, "w", encoding="utf-8") as f:
        f.write(f"{index_label},{target}\n")
        f.writelines(
            list(f"{session.session_id},{session.class_1_proba}\n" for session in predicted_labels)
        )


@click.command()
@click.argument("x_test", type=click.Path(exists=True))
@click.argument("out_submission", type=click.Path())
def build_submission(
    x_test: str,
    out_submission: str,
):
    """Builds submission file using predefined params of the best model

    Args:
        x_test (str): path to raw test dataset (csv)
        out_submission (str): path to submisstion file (csv)
    """
    cfg = get_config()

    X_test = pd.read_csv(x_test, parse_dates=cfg.data.date_cols)
    request_body = X_test.to_json(orient="records")
    response = requests.post(
        cfg.model_service.url + cfg.model_service.predict_proba_method, data=request_body
    )
    predictions = parse_obj_as(List[PredictionResponse], response.json())
    write_to_submission_file(predictions, out_submission)


if __name__ == "__main__":
    build_submission()  # pylint: disable=no-value-for-parameter
