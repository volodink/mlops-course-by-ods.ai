from typing import List

import click
import mlflow
import numpy as np
import pandas as pd
from mlflow.models.model import ModelInfo
from scipy.sparse import load_npz

from src.configuration import get_config
from src.models.evaluate import evaluate
from src.models.mlflow_helper import (STAGE_NONE, STAGE_STAGING,
                                      assign_model_stage,
                                      get_sklearn_model_version)


@click.command()
@click.argument("x_train", type=click.Path(exists=True))
@click.argument("y_train", type=click.Path(exists=True))
@click.argument("out_model_source", type=click.Path())
@click.option("--clear_history", "--ch", is_flag=True)
def optimize_model(x_train: str, y_train: str, out_model_source: str, clear_history: bool):
    """Trains several models with different hyper parameters and
    calculates train/test scores using cross validation.
    Preselects the best model, if there is no Prodcution model in mlflow.
    Saves current preselected model path in out_model_source

    Args:
        x_train (str): path to train dataset (npz)
        y_train (str): path to train target (csv)
        clear_history (bool): delete history of runs and models
        out_model_source (str): path to file with model's mlflow identifier (txt)
    """
    cfg = get_config()
    mlflow.set_tracking_uri(cfg.mlflow.tracking_uri)

    # set experiment
    experiment = mlflow.set_experiment(cfg.mlflow.lr_experiment_name)
    mlf_client = mlflow.tracking.MlflowClient()
    if clear_history:
        clear_experiment_history(experiment, mlf_client)

    # load train data
    X = load_npz(x_train)
    y = pd.read_csv(y_train)[cfg.data.target_col].values

    # cross-validate models with different hyper-parameters
    c_vals = np.linspace(1, 10, 10)
    model_params = {
        "n_jobs": 1,
        "multi_class": "ovr",
        "random_state": 17,
        "solver": "liblinear",
    }
    model_infos: List[ModelInfo] = []
    for c in c_vals:
        model_params["C"] = c
        model_info = evaluate(X, y, model_params)
        model_infos.append(model_info)

    # check, if there is a selected Production or Staging model
    # if not found, assign the best from newly evaluated and save its source in file
    if not get_sklearn_model_version(cfg.mlflow.lr_model_name):
        best_model = None
        best_roc_auc = 0
        for model in model_infos:
            roc_auc = mlf_client.get_metric_history(model.run_id, "test_roc_auc")
            if roc_auc[0].value > best_roc_auc:
                best_roc_auc = roc_auc[0].value
                best_model = model
        assign_model_stage(best_model, STAGE_STAGING)
    # dump current model in use
    model_version = get_sklearn_model_version(cfg.mlflow.lr_model_name)
    if model_version:
        with open(out_model_source, "w", encoding="utf-8") as f:
            f.write(model_version.source)


def clear_experiment_history(experiment, mlf_client):
    """Delete all previous runs in experiment
    Leaves runs, if there is a model on stage other than None

    Args:
        experiment (_type_): mlflow experiment entiry
        mlf_client (_type_): mlflow client
    """
    for run in mlflow.list_run_infos(experiment.experiment_id):
        delete_run = True
        models = mlf_client.search_model_versions(f"run_id='{run.run_id}'")
        for model in models:
            if model.current_stage == STAGE_NONE:
                mlf_client.delete_model_version(model.name, model.version)
            else:
                delete_run = False
        if delete_run:
            mlflow.delete_run(run.run_id)


if __name__ == "__main__":
    optimize_model()  # pylint: disable=no-value-for-parameter
