import pickle

import great_expectations as ge
import pandas as pd
from great_expectations.dataset import PandasDataset

from src.configuration import get_config


def test_train_format():
    """Check columns in input train dataset"""
    cfg = get_config()
    df = pd.read_csv(
        "data/raw/train_sessions.csv", index_col=cfg.data.index_col, parse_dates=cfg.data.date_cols
    )
    df_ge: PandasDataset = ge.from_pandas(df)
    expected_columns = [
        "site1",
        "time1",
        "site2",
        "time2",
        "site3",
        "time3",
        "site4",
        "time4",
        "site5",
        "time5",
        "site6",
        "time6",
        "site7",
        "time7",
        "site8",
        "time8",
        "site9",
        "time9",
        "site10",
        "time10",
        "target",
    ]
    assert (
        df_ge.expect_table_columns_to_match_ordered_list(expected_columns).success is True
    ), "train_sessions.csv has wrong columns"
    assert (
        df_ge.expect_column_values_to_be_in_set("target", [0, 1]).success is True
    ), "train_sessions.csv column target contains not only 0 and 1"


def test_train_data_exists():
    """Check that train dataset has more than 100_000 rows"""
    cfg = get_config()
    df = pd.read_csv(
        "data/raw/train_sessions.csv", index_col=cfg.data.index_col, parse_dates=cfg.data.date_cols
    )
    df_ge: PandasDataset = ge.from_pandas(df)
    assert (
        df_ge.expect_table_row_count_to_be_between(100_000, None).success is True
    ), "train dataset row count is too small"


def test_test_data_exists():
    """Check that train dataset has more than 100_000 rows"""
    cfg = get_config()
    df = pd.read_csv(
        "data/raw/test_sessions.csv", index_col=cfg.data.index_col, parse_dates=cfg.data.date_cols
    )
    df_ge: PandasDataset = ge.from_pandas(df)
    assert (
        df_ge.expect_table_row_count_to_be_between(10_000, None).success is True
    ), "test dataset row count is too small"


def test_test_format():
    """Check columns in input test dataset"""
    cfg = get_config()
    df = pd.read_csv(
        "data/raw/test_sessions.csv", index_col=cfg.data.index_col, parse_dates=cfg.data.date_cols
    )
    df_ge: PandasDataset = ge.from_pandas(df)
    expected_columns = [
        "site1",
        "time1",
        "site2",
        "time2",
        "site3",
        "time3",
        "site4",
        "time4",
        "site5",
        "time5",
        "site6",
        "time6",
        "site7",
        "time7",
        "site8",
        "time8",
        "site9",
        "time9",
        "site10",
        "time10",
    ]
    assert (
        df_ge.expect_table_columns_to_match_ordered_list(expected_columns).success is True
    ), "test_sessions.csv has wrong columns"
    assert (
        df_ge.expect_column_values_to_not_be_null("site1").success is True
    ), "test_sessions.csv site1 contains null"
    assert (
        df_ge.expect_column_values_to_not_be_null("time1").success is True
    ), "train_sessions.csv time1 contains null"


def test_train_test_types():
    """Check column types in input train and test datasets"""
    cfg = get_config()

    df_to_check = ["data/raw/train_sessions.csv", "data/raw/test_sessions.csv"]
    for df_file in df_to_check:
        df = pd.read_csv(df_file, index_col=cfg.data.index_col, parse_dates=cfg.data.date_cols)
        df_ge: PandasDataset = ge.from_pandas(df)

        assert (
            df_ge.expect_column_values_to_not_be_null("site1").success is True
        ), f"{df_file} site1 contains null"
        assert (
            df_ge.expect_column_values_to_not_be_null("time1").success is True
        ), f"{df_file} time1 contains null"
        assert (
            df_ge.expect_column_values_to_be_of_type(cfg.data.site_cols[0], "int64").success is True
        ), f"{cfg.data.site_cols[0]} in {df_file} is not int64"
        for column_name in cfg.data.site_cols[1:]:
            assert (
                df_ge.expect_column_values_to_be_of_type(column_name, "float64").success is True
            ), f"{column_name} in {df_file} is not float64"
        for column_name in cfg.data.date_cols:
            assert (
                df_ge.expect_column_values_to_be_of_type(column_name, "datetime64").success is True
            ), f"{column_name} in {df_file} is not datetime64"


def test_site_dict():
    """Check that site dictionary contains all values from datasets"""
    cfg = get_config()
    with open("data/raw/site_dic.pkl", "rb") as site_dict_file:
        site_dict = pickle.load(site_dict_file)
    df_to_check = ["data/raw/train_sessions.csv", "data/raw/test_sessions.csv"]
    for df_file in df_to_check:
        df = pd.read_csv(df_file, index_col=cfg.data.index_col, parse_dates=cfg.data.date_cols)
        df_ge: PandasDataset = ge.from_pandas(df)

        site_codes = set(site_dict.values())
        for column_name in cfg.data.site_cols:
            assert (
                df_ge.expect_column_distinct_values_to_be_in_set(column_name, site_codes).success
                is True
            ), f"column {column_name} in {df_file} contains values \
                that are not present in site_dic.pkl"


if __name__ == "__main__":
    test_train_format()
    test_test_format()
    test_site_dict()
    test_train_test_types()
    test_train_data_exists()
    test_test_data_exists()
