import great_expectations as ge
import joblib as jb
import numpy as np
import pandas as pd
import pytest
from click.testing import CliRunner
from great_expectations.dataset import PandasDataset
from scipy.sparse import load_npz
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import FeatureUnion, Pipeline

from src.configuration import get_config
from src.data.add_missing_data import add_missing_data
from src.data.build_target import build_target
from src.data.clean_data import clean_data
from src.features.build_features import build_features
from src.features.build_preferences import build_preferences
from src.features.build_sites import SitesBuilder, build_sites
from src.features.build_union import build_union
from src.features.cat_features_builder import CatFeaturesBuilder
from src.features.num_feature_builder import NumFeatureBuilder
from src.features.user_preference_features_builder import \
    UserPreferenceFeaturesBuilder

cli = CliRunner()


@pytest.mark.order(1)
def test_clean_data():
    """Checks that clean_data removes Alice's sessions on 2013-04-12.
    There are 2 sessions in test data
    """
    cfg = get_config()
    # target row number after cleaning data
    expected_row_number = 9998
    # execute clean_data
    result = cli.invoke(
        clean_data, ["data/unittest/train_sessions.csv", "data/unittest/train_sessions_clean.csv"]
    )
    # check clean_data exited without errors
    assert result.exit_code == 0, f"clean_data returned error: {result.exit_code}\n{result.output}"
    # check new row number is equal to expected
    df_clean = pd.read_csv(
        "data/unittest/train_sessions_clean.csv",
        index_col=cfg.data.index_col,
        parse_dates=cfg.data.date_cols,
    )
    assert df_clean.shape[0] == expected_row_number, "clean_data removed wrong number of rows"


@pytest.mark.order(2)
def test_add_missig_data():
    """Checks that add_missing_data copies correct number of rows"""
    cfg = get_config()
    # 8 rows are to be copied to week 49
    expected_week_49_rows = 530
    expected_row_number = 9998 + expected_week_49_rows
    # invoke add_missing_data and check result code
    result = cli.invoke(
        add_missing_data,
        [
            "data/unittest/train_sessions_clean.csv",
            "data/unittest/train_sessions_missing_added.csv",
        ],
    )
    assert (
        result.exit_code == 0
    ), f"add_missing_data returned error: {result.exit_code}\n{result.output}"
    # load processed df
    df_missing_added = pd.read_csv(
        "data/unittest/train_sessions_missing_added.csv",
        index_col=cfg.data.index_col,
        parse_dates=cfg.data.date_cols,
    )
    # check total row number
    actual_row_number = df_missing_added.shape[0]
    assert (
        actual_row_number == expected_row_number
    ), f"add_missing_data added wrong number of rows: \
        expected {expected_row_number}, got {actual_row_number}"
    # check week 49 row number
    actual_week_49_rows = df_missing_added[
        df_missing_added.time1.dt.isocalendar().week == 49
    ].shape[0]
    assert (
        actual_week_49_rows == expected_week_49_rows
    ), f"add_missing_data added wrong number of rows for week 49: \
        expected {expected_week_49_rows}, got {actual_week_49_rows}"


@pytest.mark.order(3)
def test_build_sites():
    """Checks buid_sites creates expected number of features for sites"""
    cfg = get_config()
    # check build_sites executes without errors
    result = cli.invoke(
        build_sites,
        [
            "data/unittest/train_sessions_missing_added.csv",
            "data/unittest/train_sites.npz",
            "data/unittest/build_sites_transformer.pkl",
        ],
    )
    assert result.exit_code == 0, f"build_sites returned error: {result.exit_code}\n{result.output}"
    ds_train = load_npz("data/unittest/train_sites.npz")
    # check expected minimum number of features
    df_train = pd.read_csv(
        "data/unittest/train_sessions_missing_added.csv",
        index_col=cfg.data.index_col,
        parse_dates=cfg.data.date_cols,
    )
    sites_count = np.unique(df_train[cfg.data.site_cols].to_numpy().reshape(-1)).shape[0]
    assert sites_count <= ds_train.shape[1], "train_sites does not contain enough features"
    # check site transformer type
    transformer = jb.load("data/unittest/build_sites_transformer.pkl")
    assert isinstance(transformer, Pipeline), "Sites transformer is not an sklearn Pipeline"
    assert isinstance(
        transformer[0], SitesBuilder
    ), "1st step in site transformer is not a SitesBuilder"
    assert isinstance(
        transformer[1], TfidfVectorizer
    ), "2nd step in site transformaer is not a TfidfVectorizer"
    # check transformer's result is the same as after builder method
    ds_train_alt = transformer.transform(df_train)
    count_ne = (ds_train_alt != ds_train).nnz
    assert (
        count_ne == 0
    ), "dumped sites transformer returned diffent to build_sites pipeline step result"


@pytest.mark.order(4)
def test_build_preferences():
    """Checks build_preferences creates expected number of features"""
    # invoke build_preferences on test data and check exit code
    result = cli.invoke(
        build_preferences,
        [
            "data/unittest/site_dic.pkl",
            "data/unittest/train_sessions_missing_added.csv",
            "data/unittest/train_preferences.csv",
            "data/unittest/build_preferences_transformer.pkl",
        ],
    )
    assert (
        result.exit_code == 0
    ), f"build_preferences returned error:{result.exit_code}\n{result.output}"
    # load train and test datasets
    df_train = pd.read_csv("data/unittest/train_preferences.csv", index_col=0)
    ge_train: PandasDataset = ge.from_pandas(df_train)
    # check shape
    assert (
        df_train.shape[1] == 2
    ), f"Wrong number of columns in train dataset. Expected 2, got {df_train.shape[0]}"
    # check column names
    column_names = ["pref_target", "pref_other"]
    assert (
        ge_train.expect_table_columns_to_match_ordered_list(column_names).success is True
    ), "Column names in train dataset are wrong"
    # check values
    for column in column_names:
        assert (
            ge_train.expect_column_unique_value_count_to_be_between(column, 2, 2).success is True
        ), f"train dataset contains wrong number of distinct values in column {column}"
    # check transformer type
    transformer = jb.load("data/unittest/build_preferences_transformer.pkl")
    assert isinstance(transformer, Pipeline), "Preferences transformer is not an sklearn Pipeline"
    assert isinstance(
        transformer[0], UserPreferenceFeaturesBuilder
    ), "There is not UserPreferenceFeaturesBuilder in preferences transformer"
    # check dumped transformer returns equal to pipeline results
    cfg = get_config()
    transformer = jb.load("data/unittest/build_preferences_transformer.pkl")
    df_missing_added = pd.read_csv(
        "data/unittest/train_sessions_missing_added.csv",
        index_col=cfg.data.index_col,
        parse_dates=cfg.data.date_cols,
    )
    df_train_alt = transformer.transform(df_missing_added)
    assert all(
        np.isclose(df_train, df_train_alt).reshape(-1)
    ), "Dumped preferences transformer is not equal to build_preferences pipeline step"


@pytest.mark.order(5)
def test_build_features():
    """Checks build_features creates expected number of feautures"""
    # invoke build_features and check its result code
    result = cli.invoke(
        build_features,
        [
            "data/unittest/train_sessions_missing_added.csv",
            "data/unittest/train_features.npz",
            "data/unittest/build_features_transformer.pkl",
        ],
    )
    assert (
        result.exit_code == 0
    ), f"build_features returned error {result.exit_code}:\n{result.output}"
    # check dimensions
    train_df = pd.read_csv("data/unittest/train_sessions_missing_added.csv")
    train_sparse = load_npz("data/unittest/train_features.npz")
    assert train_df.shape[0] == train_sparse.shape[0], "train dataset contains wrong number of rows"
    # check values of each feature are not always 0 in train
    for i in range(train_df.shape[1]):
        assert any(train_sparse[:, i]), f"There are only 0 in column {i} in train dataset"
    # check transformer type
    transformer = jb.load("data/unittest/build_features_transformer.pkl")
    assert isinstance(transformer, FeatureUnion), "Features transformer is not a FeatureUnion"
    assert isinstance(
        transformer.transformer_list[0][1], Pipeline
    ), "1st step in features transformer is not a Pipeline"
    assert isinstance(
        transformer.transformer_list[1][1], Pipeline
    ), "1st step in features transformer is not a Pipeline"
    assert isinstance(
        transformer.transformer_list[0][1][0], CatFeaturesBuilder
    ), "1st step in feature transformer lacks CatFeaturesBuilder"
    assert isinstance(
        transformer.transformer_list[1][1][0], NumFeatureBuilder
    ), "2nd step in feature transformer lacks NumFeatureBuilder"
    # check dumped transformer returns the same results as pipeline
    cfg = get_config()
    transformer = jb.load("data/unittest/build_features_transformer.pkl")
    ds_train = pd.read_csv(
        "data/unittest/train_sessions_missing_added.csv",
        index_col=cfg.data.index_col,
        parse_dates=cfg.data.date_cols,
    )
    train_sparse_alt = transformer.transform(ds_train)
    count_ne = (train_sparse_alt != train_sparse).nnz
    assert count_ne == 0, "Feature transformer returns different to pipeline result"


@pytest.mark.order(6)
def test_build_union():
    """Checks build_union builds expected number of features"""
    # check build_union executes without erorrs
    result = cli.invoke(
        build_union,
        [
            "data/unittest/train_sites.npz",
            "data/unittest/train_preferences.csv",
            "data/unittest/train_features.npz",
            "data/unittest/build_sites_transformer.pkl",
            "data/unittest/build_preferences_transformer.pkl",
            "data/unittest/build_features_transformer.pkl",
            "data/unittest/train_full.npz",
            "data/unittest/full_transformer.pkl",
            "--skip-mlflow",
        ],
    )
    assert result.exit_code == 0, f"build_union returned error {result.exit_code}:\n{result.output}"
    # check dimensions
    train_sites_sparse = load_npz("data/unittest/train_sites.npz")
    train_pref_df = pd.read_csv("data/unittest/train_preferences.csv", index_col=0)
    train_feat_sparse = load_npz("data/unittest/train_features.npz")
    train_full_sparse = load_npz("data/unittest/train_full.npz")
    assert (
        train_full_sparse.shape[1]
        == train_sites_sparse.shape[1] + train_pref_df.shape[1] + train_feat_sparse.shape[1]
    ), "full train dataset contains wrong number of columns"
    # check full transformer is equal to step by step process
    cfg = get_config()
    transformer: FeatureUnion = jb.load("data/unittest/full_transformer.pkl")
    X_train = pd.read_csv(
        "data/unittest/train_sessions_missing_added.csv",
        index_col=cfg.data.index_col,
        parse_dates=cfg.data.date_cols,
    )
    X_train_alt = transformer.transform(X_train)
    train_full_csr = train_full_sparse.tocsr()
    assert np.allclose(X_train_alt.data, train_full_csr.data) and all(
        X_train_alt.indices == train_full_csr.indices
    ), "Dumped full transformer is not equal to step by step process"


@pytest.mark.order(7)
def test_build_target():
    """Check that build_target produces data with correct shape and expected values"""
    cfg = get_config()
    # check build_target does not return erorr
    result = cli.invoke(
        build_target, ["data/unittest/train_sessions_missing_added.csv", "data/unittest/target.csv"]
    )
    assert (
        result.exit_code == 0
    ), f"build_target has returned error {result.exit_code}:\n{result.output}"
    # check dimensions
    train_df = pd.read_csv(
        "data/unittest/train_sessions_missing_added.csv", index_col=cfg.data.index_col
    )
    target_df = pd.read_csv("data/unittest/target.csv", index_col=0)
    assert target_df.shape[1] == 1, "target dataset contains invalid number of columns"
    assert target_df.shape[0] == train_df.shape[0], "target contains invalid number of rows"
    # check values are 0 and 1
    target_ge: PandasDataset = ge.from_pandas(target_df)
    assert (
        target_ge.expect_column_values_to_be_in_set("target", [0, 1]).success is True
    ), "target dataset contains values not equal to 0 or 1"


if __name__ == "__main__":
    test_clean_data()
    test_add_missig_data()
    test_build_sites()
    test_build_preferences()
    test_build_features()
    test_build_union()
    test_build_target()
